import time
import re
import logging


class DMM6500():
    handle = None


    measurementFunction = ['VOLT:AC', 'VOLT:DC', 'FREQ']
    triggerSources = ['HOLD', 'IMM','TIM', 'MAN', 'BUS', 'TLIN', 'EXT']
    ranges = ["auto", "10"]

    # general

    def __init__(self, handle):
        self.handle = handle
        
        self.log = logging.getLogger()

        self.handle.write("*RST") # reset device
        self.handle.write("*CLS") # clear device


    def get_id(self):
        return self.handle.query("*IDN?")

    def set_mode(self, mode):
        pass

    # Triggering

    def set_trigger_source(self, source ):
        pass

    def set_measurement_function(self, function):
        pass




    def trigger_measurement(self):
        pass



    def get_fresh(self):
        """
        See Operator manual 4.19.4
        :return:
        """
        cmd = ":data:fresh?"
        return self.handle.query(cmd)

    def is_available(self):
        """
        reads the status register of the Multimeter.
        Returns True if a new reading is available.
        After readout the value of the register is set to false
        :return: True or False
        """
        return (int(self.handle.query(":stat:meas:event?")) & (1 << 5))
    
    def set_mode1(self, func = "VOLT:DC", range = "auto"):
        cmd = f":SENS:FUNC 'VOLT:AC'"
        self.handle.write(cmd) 
        #cmd = f"SENSE:FUNC FREQ"
        #self.handle.write(cmd)        
        cmd = f":SENS1:FUNC 'FREQ'"
        self.handle.write(cmd) 
        # cmd = f":SENS1:{func}:FUNC ON"
        # self.handle.write(cmd)        

    def set_sens1(self, func = "VOLT:DC", range = "auto"):
        if func not in self.measurementFunction:
            self.log.error("Measurement %s funciton is not valid", func)
        else:
            cmd = f":SENS1:FUNC '{func}'"
            self.handle.write(cmd)
        
        if func != "FREQ":
            if range not in self.ranges:
                self.log.error("Range %s not allowed", range)
            else:
                mode = func.split(":")[0]
                cmd = f":SENS1:{func}:RANG {range}"
                self.handle.write(cmd)

    def set_sens(self, func = "VOLT:DC", range = "auto"):
        if func not in self.measurementFunction:
            self.log.error("Measurement %s funciton is not valid", func)
        else:
            cmd = f":SENS:FUNC '{func}'"
            self.handle.write(cmd)
        
        if func != "FREQ":
            if range not in self.ranges:
                self.log.error("Range %s not allowed", range)
            else:
                mode = func.split(":")[0]
                cmd = f":SENS:{func}:RANG {range}"
                self.handle.write(cmd)
            
    def set_trigger(self, mdig_cnt = 1):
        self.handle.write(f"TRIG:LOAD \"Empty\"")
        self.handle.write(f"TRIG:BLOC:WAIT 1, EXT")
        self.handle.write(f"TRIG:EXT:IN:EDGE RIS")
        self.handle.write(f"TRIG:BLOC:MDIG 2, \"defbuffer1\", {mdig_cnt}")
        self.handle.write(f"TRIG:BLOC:BRAN:ALW 3, 1")
        
    def arm_trigger(self):
        self.handle.write(f"INIT")
        #self.handle.write(f"*WAI")
            
    def parse(self, text):
        exp = "([+-]?[0-9]+[.]*[0-9]*E[+-][0-9]*)"
        match = re.search(exp, text)
        # If a match is found, extract he captured group
        if match:
            return (float(match.group(1)))
        else:
            print("Invalid values" + text)      

    def fetch(self):
        r = self.parse(self.handle.query(":FETC?"))
        return r

    def read(self, buffer = "defbuffer1"):
        
        r = self.parse(self.handle.query(f":READ? \"{buffer}\""))
        return r
    
    def meas(self, func):
        r = self.parse(self.handle.query(f":MEAS:{func}? \"defbuffer1\""))
        return r
    
    def meas1(self, func):
        r = self.parse(self.handle.query(f":MEAS:{func}? \"defbuffer2\""))
        return r


