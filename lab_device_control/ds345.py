import pyvisa as visa


class DS345:
    comm_link: type(visa.Resource)

    waveforms: list[str] = ["sine", "square", "triangle", "ramp", "noise", "arbitrary"]
    modulations: list[str] = ["lin_sweep", "log_sweep", "internal_am", "fm", "phase_mod", "burst"]
    trigger_sources: list[str] = ["single", "rate", "pos_slope", "neg_slope", "line"]

    def __init__(self, comm_link):
        self.comm_link = comm_link
        # resource_manager = visa.ResourceManager()
        # self.comm_link = resource_manager.open_resource(port)
        # self.comm_link.write_termination = "\n"

    def get_id(self):
        return self.comm_link.query("*IDN?")

    def set_amplitude(self, amplitude, unit="VP"):
        """
        :param amplitude: float value of amplitude
        :param unit: one of the following: VP (Vpp) , VR (V_RMS) , DB (dBm) , vor Arb waveforms only VP is valid
        :return:
        """
        cmd = "AMPL {}{}".format(amplitude, unit)
        self.comm_link.write(cmd)

    def set_frequency(self, frequency):
        """
        set the frequency, not suitable for noise or arb signal
        :param frequency: float , 1uHz resolution
        :return:
        """
        cmd = "FREQ {}".format(frequency)
        self.comm_link.write(cmd)

    def set_waveform(self, waveform):
        """
        If set frequency is not compatible with set waveform, the frequency will be set to max possible value and
        error is raised by the device. The error is currently not catched.
        :param waveform: one of in waveforms list
        :return:
        """
        cmd = "FUNC {}".format(self.waveforms.index(waveform.lower()))
        self.comm_link.write(cmd)

    def set_offset(self, offset):
        """
        offset plus peak amplitude must be less than 5V
        :param offset: offset in volt
        :return:
        """
        cmd = "OFFS {}".format(offset)
        self.comm_link.write(cmd)

    def set_phase(self, phase):
        """
        cant be used to set phase to zero, use clear_phase comm_linkead
        :param phase: resolution is 1mdeg , max value is 7200 - 1mdeg
        :return:
        """
        cmd = "PHSE {}".format(phase)
        self.comm_link.write(cmd)

    def clear_phase(self):
        """
        set phase to zero
        :return:
        """
        cmd = "PCLR"
        self.comm_link.write(cmd)

    def trigger(self):
        cmd = "*TRG"
        self.comm_link.write(cmd)

    def set_modulation_type(self, mod_type):
        """

        :param mod_type: one of in modulation list
        :return:
        """
        cmd = "MTYP {}".format(self.modulations.index(mod_type.lower()))
        self.comm_link.write(cmd)

    def set_enable_modulation(self, enabled):
        if enabled:
            cmd = "MENA 1"
        else:
            cmd = "MENA 0"
        self.comm_link.write(cmd)

    def set_burst_cnt(self, cnt):
        """
        burstcount shall not exceed 30000 and the burst must have a duration less than 500s
        :param cnt:
        :return:
        """
        cmd = "BCNT {}".format(cnt)
        self.comm_link.write(cmd)

    def set_trigger(self, trig_type):
        cmd = "TSRC {}".format(self.trigger_sources.index(trig_type.lower()))
        self.comm_link.write(cmd)

    def set_trigger_rate(self, rate):
        """
        sets the trigger rate for the internal rate trigger for e.g. burst triggering
        :param rate: trigger rate in Hz between 0.001 and 10k , rounded to two significant places
        :return:
        """
        cmd = f"TRAT {rate}"
        self.comm_link.write(cmd)
