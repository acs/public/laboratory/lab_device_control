import pyvisa
import coloredlogs, logging, copy, threading, uuid


class Scpi:
    
    devStruct = {
        "name" : "",
        "handle" : object,
        "dev" : object
    }
    
    resStruct = {
        "lock" : object,
        "devices" : {},
        "host" : "",
        "port" : "",
        "conType" : ""
        
    }
    
    res = {}
    
    def __init__(self, dmuObj):
        self._log = logging.getLogger(__name__)
        self._dmuObj = dmuObj
        
        logging.getLogger('pyvisa').setLevel(logging.WARN)
        
        self.rm = pyvisa.ResourceManager()


    
    
    def addRes(self, devName, host, port = None, write_termination = "\n", read_termination = "\n", query_delay = 0.1, timeout = 2000, conType = "gateway"):
        resUuid = ""
        resExists = False
        for elm in self.res:
            if self.res[elm]["host"] == host:
                self._log.info("Uri %s already in use. Device will be added to resource.", host)
                resExists = True
                resUuid = elm
                break
        
        if not resExists:
            resUuid = str(uuid.uuid1())
            self.res.update({ resUuid : copy.deepcopy(self.resStruct)})
            self.res[resUuid]["host"] = host
            
            self.res[resUuid]["lock"] = threading.Lock()
            
        
        if self.deviceExists(resUuid, devName):
            self._log.warning("Device name %s already exists for host %s", devName, host)
            return False
        
        devUuid = str(uuid.uuid1())        
        self.res[resUuid]["devices"].update({devUuid : copy.deepcopy(self.devStruct)})
        
        if conType == "gateway":
            self.res[resUuid]["devices"][devUuid]["handle"] = self.connectGateway(host, port)

        elif conType == "raw":
                self.res[resUuid]["devices"][devUuid]["handle"] = self.connectRaw(host)

        else:
            self._log.error("Con type %s not valid", conType)
            self.res[resUuid]["devices"][devUuid]["handle"] = False
            self.rmResUuid(devUuid)
            return dummyWrapper(devName)
        self.res[resUuid]["devices"][devUuid]["dev"] = scpiWrapper(devName, devUuid, self.res[resUuid]["devices"][devUuid]["handle"], self.res[resUuid])
        self.res[resUuid]["devices"][devUuid]["handle"].write_termination = write_termination
        self.res[resUuid]["devices"][devUuid]["handle"].read_termination = read_termination
        self.res[resUuid]["devices"][devUuid]["handle"].query_delay = query_delay
        self.res[resUuid]["devices"][devUuid]["handle"].timeout = timeout

        self.res[resUuid]["devices"][devUuid]["name"] = devName

        return self.res[resUuid]["devices"][devUuid]["dev"]
    
    def connectGateway(self, host, port):
        return self.rm.open_resource('TCPIP0::' + host + '::gpib0,' + str(port) + '::INSTR')
    
    def connectRaw(self, adress):
        return self.rm.open_resource(adress)
    
    def rmResUuid(self, uuid):
        [resUuid, devUuid] = self.findDev(uuid)
        if resUuid == -1:
            self._log.warning("Could not find device uuid %s", uuid)
            return -1
        if self.res[resUuid]["devices"][devUuid]["handle"]:
            self.res[resUuid]["devices"][devUuid]["handle"].close()
        del self.res[resUuid]["devices"][devUuid]["dev"]
        del self.res[resUuid]["devices"][devUuid]
        
        if not self.res[resUuid]["devices"]:
            del self.res[resUuid]
    
    def rmRes(self, dev):
        uuid = dev.getUuid()
        self.rmResUuid(uuid)

        
    def findDev(self, uuid):
        for resUuid in self.res:
            for devUuid in self.res[resUuid]["devices"]:
                if uuid == devUuid:
                    return [resUuid, devUuid]
        return (-1, -1)
    
    def deviceExists(self, resUuid, name):
        for elm in self.res[resUuid]["devices"]:
            if self.res[resUuid]["devices"][elm]["name"] == name:
                return True
            
        return False
    
    
class dummyWrapper():
    def __init__(self, name):
        self._log = logging.getLogger(__name__)
        self.name = name
        self._log.error("Constuctor: This is a non functioning dummy wrapper for %s!", self.name)

    def getUuid(self):
        self._log.error("getUuid: This is a non functioning dummy wrapper for %s!", self.name)
        
    def write(self, cmd):
        self._log.error("write: This is a non functioning dummy wrapper for %s!", self.name)
        return True

    def query(self, cmd):
        self._log.error("query: This is a non functioning dummy wrapper for %s!", self.name)
        return ""

    
class scpiWrapper:
    def __init__(self, name, devUuid, handle, res):
        self.handle = handle
        self.devUuid = devUuid
        self.name = name
        self.res = res
        
    def getUuid(self):
        return self.devUuid    
        
    def write(self, cmd):
        self.res["lock"].acquire()
        res = self.handle.write(cmd)
        self.res["lock"].release()
        
        if res > 0:
            return 1
        return -1
    
    def query(self, cmd):
        self.res["lock"].acquire()
        res = self.handle.query(cmd)
        self.res["lock"].release()
        
        return res
        