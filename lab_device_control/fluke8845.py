import pyvisa as visa
import logging, coloredlogs
import re

class Fluke8845:
    comm_link = None

    measurementModes = ['VOLT:AC', 'VOLT:DC', 'CURR:AC', 'CURR:DC', 'RES', 'FRES', 'FREQ', 'PER', 'CAP', 'TEMP', 'CONT',
                        'DIOD']
    triggerSource = ['IMM', 'EXT', 'BUS']  # Immediate, External, Bus
    calculate_Functions = ["NULL", "DB", "DBM", "AVER", "LIM"]

    # def __new__(cls, *args, **kwargs):
    #    return super().__new__(cls)

    def __init__(self, comm_link):
        self._log = logging.getLogger(__name__)
        self.comm_link = comm_link
        # self.comm_link.write_termination = "\r\n"
        self.reset()

    def get_id(self):
        return self.comm_link.query("*IDN?")

    def reset(self):
        cmd = "*RST"
        self.comm_link.write(cmd)

    def configure(self, mode: measurementModes):
        """
        Set the measuring mode of the Multimeter
        # TODO Add range / resolution as parameter
        :param mode:
        :return:
        """
        cmd = "CONF:{}".format(mode)
        self.comm_link.write(cmd)

    def set_measurement_function(self, function, channel=1):
        cmd = f"FUNC{channel} \"{function}\""
        self.comm_link.write(cmd)

    def set_range(self, function, range):
        cmd = f"{function}:RANG {range}"
        self.comm_link.write(cmd)

    def set_ac_filter(self, frequency, type="VOLT"):
        """
        :param frequency: possible parameter: 3, 20(default), 200, MIN, MAX
        :param type: [string] : VOLT or CURR
        :return:
        """
        cmd = f"{type}:AC:BAND {frequency}"
        self.comm_link.write(cmd)

    def calculate_function(self, function , state ):
        """
        :param function: Null : set the present reading as offset to zero
        :param state:
        :return:
        """
        cmd = f"CALC:STAT {int(state)} ; :CALC:FUNC {function}"
        self.comm_link.write(cmd)

    # not the zero button on the front !
    def zero(self, mode):
        """

        :param mode: valid modes : ON, OFF, ONCE
        ONCE trigger a immediately zero measurement.
        :return:
        """
        cmd= f"ZERO:AUTO {mode}"
        self.comm_link.write(cmd)

    def measure(self, mode):
        """
        Simplest measurement without much configuration controll
        # TODO Add range / resolution as parameter
        :param mode:
        :return:
        """
        cmd = "MEAS:{}?".format(mode)
        return float(self.comm_link.query(cmd))
    
    def set_trigger(self, source= "IMM", delay = 0, count = 1):
        """
        Set the trigger config of the Multimeter
        """
        #@todo add range checks for delay and count
        
        if source not in self.triggerSource:
            print("Error, trigger source is not valid")
        else:
            self.comm_link.write(f"TRIG:SOUR {source}")
            self.comm_link.write(f"TRIG:DEL {delay}")
            self.comm_link.write(f"TRIG:COUN {count}")

    def set_trigger_source(self, source):
        """
        Set the trigger source of the Multimeter
        :param source:
        :return:
        """
        self._log.warning("You are using a deprecated function set_trigger_source. Consider using set_trigger")
        cmd = "TRIG:SOUR {}".format(source)
        self.comm_link.write(cmd)

    def set_trigger_delay(self, delay):
        """

        :param delay: delay between trigger and measurement cycle in seconds max 3600
        :return: None 
        """
        self._log.warning("You are using a deprecated function set_trigger_source. Consider using set_trigger")
        cmd = "TRIG:DEL {}".format(delay)
        self.comm_link.write(cmd)

    def set_trigger_count(self, count):
        self._log.warning("You are using a deprecated function set_trigger_source. Consider using set_trigger")
        cmd = f"TRIG:COUN {count}"
        self.comm_link.write(cmd)

    def set_sample_count(self, count):
        """
        Set number of measurements taken per trigger event
        :param count:
        :return: None
        """
        cmd = f"SAMP:COUN {count}"
        self.comm_link.write(cmd)

    def set_aperture(self, time):
        """
        set aperture time for frequency measurement
        :param time: possible values: [0.01; 0.1; 1 ;"MIN" ; "MAX"] 
        :return: None
        """
        cmd = f"FREQ:APER {time}"
        self.comm_link.write(cmd)
        


    def trigger_measurement(self):
        cmd = "*TRG"
        self.comm_link.write(cmd)

    def init_measurement(self):
        """
        initilaize a measurement on next trigger signal
        :return:
        """
        self.comm_link.write("INIT")

    def read_measurement(self, channel = 1, multiple_results = False):
        cmd = f"FETCH{channel}?"
        message = self.comm_link.query(cmd)
        exp = "([+-]?[0-9]+[.]*[0-9]*E[+-][0-9]*)"
        results = re.findall(exp, message)
        if len(results) == 0:
            logging.error("fluke8845.read_measurement: no valid result returned by device")
            return -1 
        elif len(results) > 1:
            logging.warning("fluke8845.read_measurement: got multiple measurements, returned latest measurement")    
        return float(results[-1])

    def read_multi_measurements(self, function = 1):
        """same functionality as read_measurement, except its able to read multiple measurement results. 

        Args:
            function (int, optional): measurement function, main = 1, 2 = secondary. Used if e.g. V_rms and frequency is measured simultaniously. Defaults to 1.

        Returns:
            List[float]: Measured values as list of floats 
        """
        cmd = f"FETCH{function}?"
        message = self.comm_link.query(cmd)
        results = [float(v) for v in message.split(",")]
        if len(results) == 0:
            logging.error("fluke8845.read_measurement: no valid result returned by device")
            return [-1 ]
       
        return results


    def read(self):
        cmd = f"read?"
        return float(self.comm_link.query(cmd))
