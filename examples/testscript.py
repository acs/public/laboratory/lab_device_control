#!/usr/bin/env python
# Basic example script to show the functionality of this library

from common.communication_link import *
from keithley2001.keithley2001 import Keithley2001

from fluke8845.fluke8845 import Fluke8845

import pyvisa as visa

# usage with custom scpi interface
mscpiCom = scpiCom()
multi_handle = mscpiCom.register_device("12", "marty.acs-lab.eonerc.rwth-aachen.de", "12", 0, "\n")
multi_handle2 = mscpiCom.register_device("11", "marty.acs-lab.eonerc.rwth-aachen.de", "11", 0, "\n")

# usage with standard visa ResourceManager
# multi_handle = visa.ResourceManager().open_resource("TCPIP0::marty.acs-lab.eonerc.rwth-aachen.de::gpib0,12::INSTR")
multi = Keithley2001(multi_handle)

# multi_handle2 = visa.ResourceManager().open_resource("TCPIP::10.100.2.216::3490::SOCKET")
# multi_handle2.write_termination = "\r\n"
# multi_handle2.read_termination = "\r\n"
# multi_handle2.query_delay = 0.5
multi2 = Fluke8845(multi_handle2)

print(multi.get_ID())

print(multi2.get_id())
