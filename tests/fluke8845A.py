import coloredlogs, logging, time
from lab_device_control.scpi import scpi
from lab_device_control.fluke8845 import Fluke8845



from dmu.dmu import dmu
from datetime import datetime, timezone



def main():
    coloredlogs.install(level='DEBUG',
    fmt='%(asctime)s %(levelname)-8s %(name)s[%(process)d] %(message)s',
    field_styles=dict(
        asctime=dict(color='green'),
        hostname=dict(color='magenta'),
        levelname=dict(color='white', bold=True),
        programname=dict(color='cyan'),
        name=dict(color='blue')))
    logging.info("Program Start")


    dmuObj = dmu()
    scpiObj = scpi(dmuObj)
    flukeHandle = scpiObj.flukeHandle = scpiObj.addRes("Fluke8845A", "TCPIP::10.100.2.216::3490::SOCKET", 11, query_delay= 0.01, timeout=15000 ,conType="raw")
    fluke = Fluke8845(flukeHandle)

    logging.info(fluke.get_id())
    
    fluke.set_measurement_function("VOLT:AC", 1)
    fluke.set_measurement_function("FREQ", 2)

    fluke.set_aperture(0.1)

    fluke.set_range("VOLT:AC", 10)
    fluke.set_range("FREQ:VOLT", 10)

    fluke.set_ac_filter(20, "VOLT")
    fluke.zero("OFF") # could be 0
    
    fluke.set_trigger("IMM")


    fluke.set_sample_count(1)
        
    while True:
        start = time.time()
        fluke.init_measurement()
        fetch1 = time.time()
        v_rms = fluke.read_measurement(1)
        fetch2 = time.time()
        f_rms = fluke.read_measurement(2)
        logging.info(f"rms: {float(v_rms)} ; frequency: {float(f_rms)} ; fetch1: {time.time() - fetch1} ; fetch2: {time.time() - fetch2} ; total: {time.time() - start}")


if __name__=="__main__":
    main()