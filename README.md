# Lab Device Control

## Description
This Project collects Python interfaces to some measurement devices in the lab. 

## Installation
For Linux Systems just clone the repro and run the makefile : 

    git clone git@git-ce.rwth-aachen.de:acs/internal/lab/devices/lab-device-control.git
    cd lab-devices-control
    make init
    
    

## Usage
example file which demonstrate connection with two devices can be found in testscript.py
    
    source vEnv/bin/acticate 
    sudo chmod +x testscript.py
    ./testscript.py
    

## Roadmap
Idea is to offer an interface to connect the measurement device in the lab to villas. First Step is to implement a python interface for the measurement devices 


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT License  
  
  
  