SHELL := /bin/bash

init:
	python3 ./createEnv.py
	source vEnv/bin/activate && \
	pip install --upgrade pip && \
	pip install .
update:
	source vEnv/bin/activate && \
	pip install .
clean:
	rm -R -f vEnv
	rm -R -f __pycache__
	rm -R -f *.egg-info
devel: init
	source vEnv/bin/activate
	pip install --no-deps -e ./

develupdate: update
	source vEnv/bin/activate
	pip install --no-deps -e ./

